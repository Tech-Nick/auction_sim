import unittest
from src.auction import Auction
from src.bid_handler import BidHandler

class TestAuction(unittest.TestCase):
 

    def test_update_amount_greater(self):
        #test update when amount is greater than self.amount
        self.account_id = 14
        self.amount = 1000
        self.auction_id = 5
        self.time_unit = 10

        Auction.update(self, 10, 2000, 6, 12)

        self.assertEqual(self.account_id,10)
        self.assertEqual(self.amount,2000)
        self.assertEqual(self.auction_id,6)
        self.assertEqual(self.time_unit,12)

    def test_update_amount_lower(self):
        #test update when amount is lower than self.amount
        self.account_id = 14
        self.amount = 1000
        self.auction_id = 5
        self.time_unit = 10

        Auction.update(self, 8, 200, 9, 14)

        self.assertEqual(self.account_id,14)
        self.assertEqual(self.amount,1000)
        self.assertEqual(self.auction_id,5)
        self.assertEqual(self.time_unit,10)

    def test_update_amount_equal(self):
        #test update when amount is the same as self.amount
        self.account_id = 14
        self.amount = 1000
        self.auction_id = 5
        self.time_unit = 10

        Auction.update(self, 3, 1000, 2, 40)

        self.assertEqual(self.account_id,14)
        self.assertEqual(self.amount,1000)
        self.assertEqual(self.auction_id,5)
        self.assertEqual(self.time_unit,10)


if __name__ == '__main__':
    unittest.main()
