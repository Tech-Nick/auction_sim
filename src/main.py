from src.data import AuctionData
from src.bid_handler import BidHandler
from src.validation import Validation

def main() -> None:
    """
    Runs the main program.

    :return: None
    """
  
    data_handler = AuctionData()
    bid_handler = BidHandler()

    #validate data scraped from csv
    print("\nThe following bids were not accpeted\n")
    for data in data_handler.data:
        #check incoming bid account id for incorrect range
        if(Validation.validate(bid_handler, data.account_id, 0, 11, "account id")):pass
        else:
            continue
       #check incoming bid ammount for incorrect range
        if(Validation.validate(bid_handler, data.amount, 0, 20001, "amount")):
            pass
        else:
            continue
        #check incoming bid auction id for incorrect range
        if(Validation.validate(bid_handler, data.auction_id, 0, 21, "auction id")):
            pass
        else:
            continue        
        #check incoming bid time unit for incorrect range
        if(Validation.validate(bid_handler, data.time_unit, 0, 101, "time unit")):
            pass
        else:
            continue 
        bid_handler.process(data.account_id, data.amount, data.auction_id, data.time_unit)
       
    #loop through each auction taking place and print the winning account id
    print("\nWinning Acount ids, amount , auction id and time\n")
    for auction in bid_handler.auction_list:
        print(bid_handler.get_winners(auction.auction_id))

if __name__ == "__main__":
    main()
