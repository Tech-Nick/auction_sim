class Validation:
    """ This class is responsible for validating bids """

    #check for wrong value and type errors
    def validate(self, id, x, y, name):
        try:
            if not id in range(x, y):
               raise ValueError
        except ValueError:
            print('value error : '+name+' "'+str(id)+'" not within acceptable range of "'+str(x)+'" to "'+str(y)+'".')
        except TypeError:
            print('type error : '+name+' expecting parameter format validate(int, int, int, string) found  validate('+str(type(id))+', '+str(type(x))+', '+str(type(y))+', '+str(type(name))+') ".')
        else:
            return True
