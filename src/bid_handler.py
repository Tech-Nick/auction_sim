from src.auction import Auction

class BidHandler:
    """
    This class is responsible for processing bids

    Attributes:
        account_id (int): the ID of the account making a bid
        amount (int): the amount that the bid was for
        auction_id (int): the ID of the auction that the bid was on
        time_unit (int): time unit of when the bid was made
    """
    #hold all the auctions
    auction_list = []
    #hold all the auction ids
    auction_ids = []
 
    #process each bid from the incoming auctions 
    def process(self, account_id: int, amount: int, auction_id: int, time_unit: int):

        self.account_id = account_id
        self.amount = amount
        self.auction_id = auction_id
        self.time_unit = time_unit

        #to start with we have to add the first auction manually
        if len(self.auction_list) < 1:
            self.auction_list.append(Auction(self.account_id, self.amount, self.auction_id, self.time_unit))
            self.auction_ids.append(self.auction_id)

        #check if the bid belongs to an existing auction
        for auction in self.auction_list:
            if self.auction_id in self.auction_ids:
                if self.auction_id == auction.auction_id:
                    auction.update(self.account_id, self.amount, self.auction_id, self.time_unit)
            else:
                #add the auction to the auction_list
                self.auction_list.append(Auction(self.account_id, self.amount, self.auction_id, self.time_unit))
                #add the auction ids to the auction_ids
                self.auction_ids.append(self.auction_id)

   #retrive the winning account id, based on a given auction id provided
    def get_winners(self, auctid):
        for auction in self.auction_list:
            if auctid == auction.auction_id:
                return auction.account_id, auction.amount, auction.auction_id, auction.time_unit

        return None