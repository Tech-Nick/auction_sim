class Auction:
    """
    This class is responsible for storing auction details including highest bid

    Attributes:
        account_id (int): the ID of the account making a bid
        amount (int): the amount that the bid was for
        auction_id (int): the ID of the auction that the bid was on
        time_unit (int): time unit of when the bid was made
    """
   
    def __init__(self, account_id: int, amount: int, auction_id: int, time_unit: int) -> None:
        """
        The constructor for the Auction class.

        :param account_id: (int) the ID of the account making a bid
        :param amount: (int) the amount that the bid was for
        :param auction_id: (int) the ID of the auction that the bid was on
        :param time_unit: (int) time unit of when the bid was made
        """
        self._account_id = account_id
        self._amount = amount
        self._auction_id = auction_id
        self._time_unit = time_unit  

    #properties
    @property
    def account_id(self): 
        return self._account_id
    
    @property
    def amount(self): 
        return self._amount

    @property
    def auction_id(self): 
        return self._auction_id

    @property
    def time_unit(self): 
        return self._time_unit    
    
    @account_id.setter
    def account_id(self, newvalue):
        self._account_id = newvalue

    @amount.setter
    def amount(self, newvalue): 
        self._amount = newvalue

    @auction_id.setter
    def auction_id(self, newvalue): 
        self._auction_id = newvalue 

    @time_unit.setter
    def time_unit(self, newvalue): 
        self._time_unit = newvalue 

    #replace the current bid with the new one if the new bid has a higher amount
    def update(self, accid, amount, aucid, time):
        if self.amount >= amount:
            return
        else:
            self.account_id = accid
            self.amount = amount
            self.auction_id = aucid
            self.time_unit = time
        return