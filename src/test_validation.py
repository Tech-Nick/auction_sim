import unittest
from src.validation import Validation

class TestValidation(unittest.TestCase):
    
    def test_validate_within_range(self):
        #test number within range
        self.assertEqual(Validation.validate(self, 10, 0, 11, "account id"),True)

    def test_validate_TypeErrors_first_param(self):
        #test for TypeErrors (first param)
        self.assertRaises(TypeError, Validation.validate ,11, 0, 11, "account id")
        self.assertRaises(TypeError, Validation.validate ,-1, 0, 11, "account id")
        self.assertRaises(TypeError, Validation.validate ,"d", 0, 31, "account id")
        self.assertRaises(TypeError, Validation.validate ,1.0, 0, 31, "account id")
        self.assertRaises(TypeError, Validation.validate ,None, 0, 31, "account id")
        self.assertRaises(TypeError, Validation.validate ,True, 0, 31, "account id")

    def test_validate_TypeErrors_second_param(self):
        #test for TypeErrors (second param)
        self.assertRaises(TypeError, Validation.validate ,5, 6, 11, "account id")
        self.assertRaises(TypeError, Validation.validate ,5, -1, 11, "account id")
        self.assertRaises(TypeError, Validation.validate ,5, "n", 31, "account id")
        self.assertRaises(TypeError, Validation.validate ,5, 0.9, 31, "account id")
        self.assertRaises(TypeError, Validation.validate ,5, None, 31, "account id")
        self.assertRaises(TypeError, Validation.validate ,5, False, 31, "account id")

    def test_validate_TypeErrors_third_param(self):
        #test for TypeErrors (third param)
        self.assertRaises(TypeError, Validation.validate ,5, 0, 4, "account id")
        self.assertRaises(TypeError, Validation.validate ,5, 0, -6, "account id")
        self.assertRaises(TypeError, Validation.validate ,5, 0, "h", "account id")
        self.assertRaises(TypeError, Validation.validate ,5, 0, 45.9, "account id")
        self.assertRaises(TypeError, Validation.validate ,5, 0, None, "account id")
        self.assertRaises(TypeError, Validation.validate ,5, 0, True, "account id")

    def test_validate_TypeErrors_fourth_param(self):
        #test for TypeErrors (fourth param)
        self.assertRaises(TypeError, Validation.validate ,5, 0, 11, 5)
        self.assertRaises(TypeError, Validation.validate ,5, 0, 11, -1)
        self.assertRaises(TypeError, Validation.validate ,5, 0, 31, 12.8)
        self.assertRaises(TypeError, Validation.validate ,5, 0, 31, 45.9)
        self.assertRaises(TypeError, Validation.validate ,5, 0, 31, None)
        self.assertRaises(TypeError, Validation.validate ,5, 0, 31, True)

if __name__ == '__main__':
    unittest.main()
